const path = require(`path`);
const slash = require(`slash`);

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  // we use the provided allContentfulBlogPost query to fetch the data from Contentful
  return graphql(
    `
      {
        allContentfulThing(filter: {types: {elemMatch: {name: {}, contentfulid: {eq: "band"}}}}) {
          edges {
            node {
              aliases
            }
          }
        }
      }
    `
  ).then(result => {
      if (result.errors) {
        console.log("Error retrieving contentful data", result.errors);
      }

      // Resolve the paths to our template
      const bandPage = path.resolve("./src/templates/band.js");

      // Then for each result we create a page.
      result.data.allContentfulThing.edges.forEach(edge => {
        edge.node.aliases.forEach(alias => {
          createPage({
            path: `/${alias.replace(/\s/g, '+')}/`,
            component: slash(bandPage),
            context: {
              name: alias
            }
          });
        });
      });
    })
    .catch(error => {
      console.log("Error retrieving contentful data", error);
    });
};
