/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  plugins: [
    `gatsby-plugin-less`,
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: `vehma4djbwyq`,
        accessToken: `mfWqAsKpZ11fm6pIyBL3Hke30_r3PMMz9LeWe_U0Jws`
      }
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        // CommonMark mode (default: true)
        commonmark: true,
        // Footnotes mode (default: true)
        footnotes: true,
        // Pedantic mode (default: true)
        pedantic: true,
        // GitHub Flavored Markdown mode (default: true)
        gfm: true,
        // Plugins configs
        plugins: [],
      },
    },
  ],
  siteMetadata: {
    title: `Does This Band Suck?`,
    titleTemplate: `%s - Does This Band Suck?`,
    description: `This site objectively answers the important question: Does This Band Suck?`,
  },
}
