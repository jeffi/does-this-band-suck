import React, { useState } from "react"
import { navigate } from "gatsby"
import Layout from "../components/Layout"
import Content from "../components/Content"
import Input from "../components/Input"
import SEO from "../components/Seo"

const Index = () => {
  const [state, setState] = useState({
    band: ''
  });

  const handleSubmit = e => {
    e.preventDefault();

    navigate(`/${state.band.toLowerCase().replace(/\s/g, '+')}`);
  }

  const handleInputChange = e => {
    setState({
      ...state,
      band: e.target.value
    });
  }

  return (
    <Layout>
      <SEO
        title="Home"
      />
      <Content>
        <form
          class="form-group"
          onSubmit={handleSubmit}
        >
          <Input
            placeholder={`Search for a band...`}
            onChange={handleInputChange}
            fullWidth
          />
        </form>
      </Content>
    </Layout>
  );
}

export default Index
