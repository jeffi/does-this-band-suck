import React from "react"
import Layout from "../components/Layout"
import Content from "../components/Content"
import SEO from "../components/Seo"

const Error = () => {
  return (
    <Layout>
      <SEO
        title="Error"
      />
      <Content>
        <h1>I have never heard of this band</h1>
        <p>Maybe you want to tell me more about it?</p>
        <p>Especially if it sucks or not</p>
      </Content>
    </Layout>
  );
};

export default Error;
