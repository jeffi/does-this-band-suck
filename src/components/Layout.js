import React from "react"
import PropTypes from "prop-types"

import Footer from "./Footer"
import Header from "./Header"

import "../styles/main.less"

const Layout = (props) => {
  return (
    <main class="main">
      <Header />
      {props.children}
      <Footer />
    </main>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
}

export default Layout
