import React from "react"

const Footer = () => (
  <footer class="footer">
    Made with <span class="emoji" role="img" aria-label="Black heart">🖤</span> by <span class="emoji" role="img" aria-label="Upside down smiley">🙃</span>
  </footer>
)

export default Footer
