import React from "react"
import PropTypes from "prop-types"

const Input = (props) => (
  <input
    className={`form-input ${props.fullWidth ? 'form-input--full-width' : ''}`}
    type={props.type}
    name={props.name}
    placeholder={props.placeholder}
    value={props.value}
    onChange={props.onChange}
    autocomplete="off"
  />
)

Input.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  fullWidth: PropTypes.bool
}

Input.defaultProps = {
  name: 'name',
  type: 'text'
}

export default Input
