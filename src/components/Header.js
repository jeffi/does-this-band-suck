import React from "react"
import { Link } from "gatsby"

const Header = () => (
  <header class="header">
    <Link
      to="/"
      className="header__title"
    >
      Does This Band Suck?
    </Link>
  </header>
)

export default Header
