import React, { useState } from "react"
import { graphql, navigate } from "gatsby"
import Layout from "../components/Layout"
import Content from "../components/Content"
import Input from "../components/Input"
import SEO from "../components/Seo"

const Band = ({ data }) => {
  const { fullName, imageUrl, description, types } = data.allContentfulThing.edges[0].node;

  const firstType = types[0];

  const [state, setState] = useState({
    band: ''
  });

  const handleSubmit = e => {
    e.preventDefault();

    navigate(`/${state.band.toLowerCase().replace(/\s/g, '+')}`);
  }

  const handleInputChange = e => {
    setState({
      ...state,
      band: e.target.value
    });
  }

  return (
    <Layout
      headerTitle={firstType.name}
      headerLink={`/${firstType.contentfulid}`}
    >
      <SEO
        title={fullName}
      />
      <Content>
        <form
          class="form-group"
          onSubmit={handleSubmit}
        >
          <Input
            placeholder={`Search for a ${firstType.contentfulid}...`}
            onChange={handleInputChange}
            fullWidth
          />
        </form>
        <h1>{fullName}</h1>
        <img
          class="content__image"
          src={imageUrl}
          alt={fullName}
        />
        <div
          class="content__description"
          dangerouslySetInnerHTML={{
            __html: description.childMarkdownRemark.html,
          }}
        />
      </Content>
    </Layout>
  );
};

export default Band;

export const query = graphql`
  query($name: String!) {
    allContentfulThing(filter: {aliases: {eq: $name}}, limit: 1) {
      edges {
        node {
          description {
            description
            childMarkdownRemark {
              html
            }
          }
          types {
            contentfulid
            name
          }
          fullName
          isThisGood
          imageUrl
        }
      }
    }
  }
`;
